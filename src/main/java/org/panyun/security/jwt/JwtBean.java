/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panyun.security.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JWT管理对象.
 *
 * @author Pan Yun
 */
public class JwtBean {

    private final KeyStoreBean keyStoreBean;

    private static final Logger LOG = Logger.getLogger(JwtBean.class.getName());

    public JwtBean(KeyStoreBean keyPairStoreBean) {
        this.keyStoreBean = keyPairStoreBean;
    }

    /**
     * 创建对称密钥签名的JWT.
     *
     * @param payload JWT中的Payload部分
     * @param alias 保存在KeyStore中，用于签名的密钥别名
     * @param keyPwd 访问KeyStore中指定密钥的密码
     * @return 签名后的JWT
     */
    public String createSecretKeySignedJws(String payload, String alias, String keyPwd) {
        Key secretKey = keyStoreBean.retrieveSecretKey(alias, keyPwd);
        // 构造JWS 
        Map header = new HashMap();
        header.put("typ", "JWT");
        header.put("alg", "HS256");
        String jws = Jwts.builder()
                .setHeader(header)
                .setPayload(payload)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
        return jws;
    }

    /**
     * 验证JWS的签名是否正确.
     *
     * @param jws 待验证的JWS
     * @param alias 用于验证的密钥对别名
     * @param keyPwd 访问密钥对的密码
     * @return 正确-true; 错误-false
     */
    public boolean validateSecretKeySignedJws(String jws, String alias, String keyPwd) {
        boolean isValid = false;
        Key key = keyStoreBean.retrieveSecretKey(alias, keyPwd);
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(jws); //解析JWS, 如果解析出错，会抛出异常.
            isValid = true;
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | IllegalArgumentException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return isValid;
    }

    /**
     * 创建私钥签名的JWT.
     *
     * @param payload JWT中的Payload部分
     * @param alias 保存在KeyStore中，用于签名的密钥对别名
     * @param keyPwd 访问KeyStore中指定密钥对的密码
     * @return 签名后的JWT
     */
    public String createPrivateKeySignedJws(String payload, String alias, String keyPwd) {
        KeyPair keyPair = keyStoreBean.retrieveKeyPair(alias, keyPwd);
        PrivateKey privateKey = keyPair.getPrivate();
        Map<String, Object> header = new HashMap<>();
        header.put("typ", "JWT");
        header.put("alg", "RS256");
        String jws = Jwts.builder()
                .setHeader(header)
                .setPayload(payload)
                .signWith(SignatureAlgorithm.RS256, privateKey)
                .compact();
        return jws;
    }

    /**
     * 验证JWS的签名是否正确.
     *
     * @param jws 待验证的JWS
     * @param alias 用于验证的密钥对别名
     * @param keyPwd 访问密钥对的密码
     * @return 正确-true; 错误-false
     */
    public boolean validatePrivateKeySignedJws(String jws, String alias, String keyPwd) {
        boolean isValid = false;
        KeyPair keyPair = keyStoreBean.retrieveKeyPair(alias, keyPwd);
        PublicKey publicKey = keyPair.getPublic();
        try {
            Jwts.parser().setSigningKey(publicKey).parseClaimsJws(jws);
            isValid = true;
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | IllegalArgumentException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return isValid;
    }
    
    /**
     * 返回JWS解析器.
     * @param alias
     * @param keyPwd
     * @return JwtParser
     * @since 0.1.1
     */
    public JwtParser getParser(String alias, String keyPwd) {
        KeyPair keyPair = keyStoreBean.retrieveKeyPair(alias, keyPwd);
        PublicKey publicKey = keyPair.getPublic();
        JwtParser parser = null;
        try {
            parser = Jwts.parser().setSigningKey(publicKey);
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | IllegalArgumentException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return parser;
    }

    /**
     * 测试方法.
     * @param args 
     */
    public static void main(String[] args) {
        KeyStoreBean kps = new KeyStoreBean("test.ks", "123456");
        JwtBean jb = new JwtBean(kps);
        String payload = "{\"iss\":\"panyun.org\",\"exp\":2300819380,\"fnm\":\"Pan Yun\"}";

        String jwsAes = jb.createSecretKeySignedJws(payload, "aes-key", "654321");
        System.out.println("JWS_aes: " + jwsAes);
        System.out.println("Is jws-aes valid: " + jb.validateSecretKeySignedJws(jwsAes, "aes-key", "654321"));

        String jwsRsa = jb.createPrivateKeySignedJws(payload, "rsa-key", "654321");
        System.out.println("JWS-rsa: " + jwsRsa);
        System.out.println("Is jws-rsa valid: " + jb.validatePrivateKeySignedJws(jwsRsa, "rsa-key", "654321"));
    }

}
