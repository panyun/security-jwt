/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panyun.security.jwt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JCEKS类型的KeyStore密钥存储库管理.
 * <p>
 * KeyStore可用以下命令生成（默认为JKS类型，只能存储非对称密钥；JCEKS类型的可以存储对称和非对称密钥）：
 * <p>
 * keytool -genkeypair -alias mykey -keyalg RSA -keystore keypair.jks -keysize
 * 2048 -storepass mystorepass -keypass mykeypass
 * <p>
 * keytool -genkey -alias mykey -dname "CN=Pan Yun" -keystore mykeystore -keyalg
 * RSA -keysize 2048 -keypass mykeypass -storepass mystorepass -storetype JCEKS
 * -validity 999
 * <p>
 * 生成存储对称密钥(AES算法)的KeyStore：
 * <p>
 * keytool -genseckey -alias mykey -keystore mykeystore -storetype JCEKS
 * -storepass mystorepass -keyalg AES -keysize 256 -keypass mykeypass
 *
 * @author Pan Yun
 */
public class KeyStoreBean {

    private static final Logger LOG = Logger.getLogger(KeyStoreBean.class.getName());

    private final String KEYSTORE_FILE;
    private final String KEYSTORE_PWD;

    /**
     * 构造方法.
     *
     * @param keyStoreFile KeyStore文件名
     * @param keyStorePwd KeyStore库密码
     *
     */
    public KeyStoreBean(String keyStoreFile, String keyStorePwd) {
        this.KEYSTORE_FILE = keyStoreFile;
        this.KEYSTORE_PWD = keyStorePwd;
    }

    /**
     * 从KeyStore中取出对称密钥.
     *
     * @param alias Key条目的别名
     * @param keyPwd Key条目密码
     * @return 对称密钥
     */
    public Key retrieveSecretKey(String alias, String keyPwd) {
        Key secretKey = null;
        KeyStore keyStore = getKeyStore();

        PasswordProtection pwdProtection = new PasswordProtection(keyPwd.toCharArray());
        try {
            KeyStore.Entry entry = keyStore.getEntry(alias, pwdProtection);
            secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
        } catch (NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException ex) {
            Logger.getLogger(KeyStoreBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return secretKey;
    }

    /**
     * 从KeyStore中取出密钥对.
     *
     * @param alias Key条目的别名
     * @param keyPwd Key条目密码
     * @return KeyPair非对称密钥对
     */
    public KeyPair retrieveKeyPair(String alias, String keyPwd) {
        KeyPair keyPair = null;
        KeyStore keyStore = getKeyStore();
        try {
            Key key = keyStore.getKey(alias, keyPwd.toCharArray());
            if (key instanceof PrivateKey) {
                Certificate cert = keyStore.getCertificate(alias);
                PublicKey publicKey = cert.getPublicKey();
                keyPair = new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return keyPair;
    }

    /**
     * 获取保存密钥的KeyStore.
     *
     * @return KeyStore
     */
    private KeyStore getKeyStore() {
        KeyStore keyStore = null;
        try {
            File keyStoreFile = new File(KEYSTORE_FILE);
            keyStore = KeyStore.getInstance("jceks"); //必须与创建的KeyStore的类型匹配。默认使用jks，建议用jceks。
            keyStore.load(new FileInputStream(keyStoreFile), KEYSTORE_PWD.toCharArray());
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return keyStore;
    }

    /**
     * 测试方法.
     * @param args 
     */
    public static void main(String[] args) {
        System.out.println("Current working directory: " + System.getProperty("user.dir"));
        KeyStoreBean ks = new KeyStoreBean("test.ks", "123456");
        // 取出密钥对
        KeyPair kp = ks.retrieveKeyPair("rsa-key", "654321");
        Key pubKey = kp.getPublic();
        Key priKey = kp.getPrivate();
        System.out.println("PublicKey:" + pubKey);
        System.out.println("PrivateKey:" + priKey);
        //取出对称密钥
        Key sk = ks.retrieveSecretKey("aes-key", "654321");
        System.out.println("SecretKey: " + sk);
    }

}
